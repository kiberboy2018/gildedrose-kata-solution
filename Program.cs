﻿using csharpcore.models;
using System;
using System.Collections.Generic;

namespace csharpcore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /**
             * Backstage pass increases in Quality as its SellIn value approaches; Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but; Quality drops to 0 after the concert
             * Aged brie actually increases in Quality the older it gets
             * Standard item Once the sell by date has passed, Quality degrades twice as fast - The Quality of an item is never negative
             * Sulfuras never has to be sold or decreases in Quality
             * Conjured items degrade in Quality twice as fast as normal items
             * The Quality of an item is never more than 50    , kodel sulfuras pavyzdyje turi kokybes reiksme 80? Juk jis toks pat item'as, nepamineta, kad jo kokybe gali buti didesne 
             */



            Console.WriteLine("OMGHAI!");

            IList<Item> Items = new List<Item>{
                new Item {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 , Type = ItemType.Standard},
                new Item {Name = "Aged Brie", SellIn = 2, Quality = 0, Type = ItemType.AgedBrie},
                new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 , Type = ItemType.Standard},
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 , Type = ItemType.Sulfur},
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = -1, Quality = 80, Type = ItemType.Sulfur},
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 15,
                    Quality = 20,
                    Type= ItemType.BackstageTicket
                },
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 10,
                    Quality = 49,
                    Type= ItemType.BackstageTicket
                },
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 5,
                    Quality = 49,
                    Type= ItemType.BackstageTicket
                },
				// this conjured item does not work properly yet
				new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6, IsConjured=true, Type = ItemType.Standard}
            };

            var app = new GildedRose(Items);


            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                for (var j = 0; j < Items.Count; j++)
                {
                    System.Console.WriteLine(Items[j].Name + ", " + Items[j].SellIn + ", " + Items[j].Quality);
                }
                Console.WriteLine("");
                app.UpdateQuality();
            }
        }
    }
}
