﻿namespace csharpcore.models
{
    public class Item
    {
        public string Name { get; set; }
        public int SellIn { get; set; }
        public int Quality { get; set; }
        public ItemType Type { get; set; }
        public bool IsConjured { get; set; }

        public void IncreaseQualityValue(int increment = 1)
        {
            var increaseBy = SellIn < 0
                ? increment * 2
                : increment;
            if (Quality + increaseBy > 50)
            {
                Quality = 50;
            }
            else
            {
                Quality += increaseBy;
            }

        }

        public void DecreaseQualityValue(int decrease = 1)
        {
            var decreaseBy = (SellIn < 0
                ? 2
                : decrease) * (IsConjured ? 2 : 1);

            if (Quality - decreaseBy < 0)
            {
                Quality = 0;
            }
            else
            {
                Quality -= decreaseBy;
            }
        }

    }



}
