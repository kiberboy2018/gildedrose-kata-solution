﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharpcore
{
    public enum ItemType
    {
        Standard = 1,
        AgedBrie = 2,
        BackstageTicket = 3,
        Sulfur = 4
    }
}
