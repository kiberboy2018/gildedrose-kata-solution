﻿using Xunit;
using System.Collections.Generic;
using csharpcore.models;
using System.Linq;
using System;

namespace csharpcore
{
    public class GildedRoseTest
    {
        /**
         * Standard Item tests
         **/
        [Fact]
        public void UpdateQuality_QualityValueShouldBeZeroForStandardItem()
        {
            var Items = new List<Item>{
                new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 , Type = ItemType.Standard},
            };
            GildedRose app = new GildedRose(Items);

            for (int i = 0; i < 7; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();

            Assert.Equal(0, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SellinValueShouldBeNegativeForStandardItem()
        {
            var Items = new List<Item>{
                new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 , Type = ItemType.Standard},
            };
            GildedRose app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();

            Assert.Equal(-5, item.SellIn);
        }

        /**
        * Sulfuras tests
        **/
        [Fact]
        public void UpdateQuality_QualityValueShouldStayTheSameForSulfuras()
        {
            var Items = new List<Item>{
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 , Type = ItemType.Sulfur},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();

            Assert.Equal(80, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SellinValueShouldStayTheSameForSulfuras()
        {
            var Items = new List<Item>{
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 , Type = ItemType.Sulfur},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(0, item.SellIn);
        }

        /**
         * Aged brie tests
         **/
        [Fact]
        public void UpdateQuality_QualityValueShouldIncreaseToTenInTenDaysForAgedBrie()
        {
            var Items = new List<Item>{
                new Item {Name = "Aged Brie", SellIn = 10, Quality = 0, Type = ItemType.AgedBrie},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(10, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SellinValueShouldBeZeroForAgedBrie()
        {
            var Items = new List<Item>{
                new Item {Name = "Aged Brie", SellIn = 10, Quality = 0, Type = ItemType.AgedBrie},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(0, item.SellIn);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldIncreaseToTwentyInFifteenDaysForAgedBrie()
        {
            var Items = new List<Item>{
                new Item {Name = "Aged Brie", SellIn = 10, Quality = 0, Type = ItemType.AgedBrie},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 15; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(20, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SellinValueShouldBeNegativeForAgedBrie()
        {
            var Items = new List<Item>{
                new Item {Name = "Aged Brie", SellIn = 5, Quality = 0, Type = ItemType.AgedBrie},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 10; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(-5, item.SellIn);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldNotPassFiftyForAgedBrie()
        {
            var Items = new List<Item>{
                new Item {Name = "Aged Brie", SellIn = 10, Quality = 0, Type = ItemType.AgedBrie},
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 35; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(50, item.Quality);
        }

        /**
         * Backstage ticket tests 
         **/
        [Fact]
        public void UpdateQuality_QualityValueShouldIncreaseToFiveWhenSellInIsMoreThanTenForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 15,
                    Quality = 0,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(5, item.Quality);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldIncreaseToTenWhenSellInIsBetweenFiveAndTenForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 10,
                    Quality = 0,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(10, item.Quality);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldIncreaseToFifteenWhenSellInIsBetweenFiveAndZeroForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 5,
                    Quality = 0,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(15, item.Quality);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldBeZeroWhenSellInIsLessThanZeroForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 2,
                    Quality = 0,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(0, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SellinValueShouldBeFiveForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn =10,
                    Quality = 0,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(5, item.SellIn);
        }

        [Fact]
        public void UpdateQuality_QualityValueShouldNotExceedFiftyForBackstageTicket()
        {
            var Items = new List<Item>{
                    new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn =10,
                    Quality = 40,
                    Type= ItemType.BackstageTicket
                },
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 9; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(50, item.Quality);
        }

        /**
        * Conjured item tests 
        **/
        [Fact]
        public void UpdateQuality_QualityValueShouldDecreaseToFiveForConjuredItem()
        {
            var Items = new List<Item>{
                   new Item {Name = "Conjured Mana Cake", SellIn = 10, Quality = 15, IsConjured=true, Type = ItemType.Standard}
            };
            var app = new GildedRose(Items);

            for (int i = 0; i < 5; i++)
            {
                app.UpdateQuality();
            }
            var item = app.Items.FirstOrDefault();
            Assert.Equal(5, item.Quality);
        }
    }
}