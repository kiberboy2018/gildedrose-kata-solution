﻿using csharpcore.handlers;
using csharpcore.models;
using System.Collections.Generic;

namespace csharpcore
{
    public class GildedRose
    {
        public IList<Item> Items;

        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            AbstractHandler h1 = new AgedBrieHandler();
            AbstractHandler h2 = new BackstageTicketHandler();
            AbstractHandler h3 = new SulfurHandler();
            AbstractHandler h4 = new StandardItemHandler();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3);
            h3.SetSuccessor(h4);
            foreach (var item in Items)
            {
                h1.HandleItem(item);
            }
        }
    }
}
