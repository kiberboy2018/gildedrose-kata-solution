﻿using csharpcore.models;

namespace csharpcore.handlers
{
    public class AgedBrieHandler : AbstractHandler
    {
        public override void HandleItem(Item request)
        {
            if (request.Type == ItemType.AgedBrie)
            {
                request.SellIn--;
                request.IncreaseQualityValue();
            }
            else
            {
                nextHandler.HandleItem(request);
            }
        }
    }
}
