﻿using csharpcore.models;

namespace csharpcore.handlers
{
    public class SulfurHandler : AbstractHandler
    {
        public override void HandleItem(Item request)
        {
            if (request.Type == ItemType.Sulfur)
            {
                //Do nothing, cuz its legendary
            }
            else
            {
                nextHandler.HandleItem(request);
            }
        }
    }
}
