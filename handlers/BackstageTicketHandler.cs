﻿using csharpcore.models;

namespace csharpcore.handlers
{
    public class BackstageTicketHandler : AbstractHandler
    {
        public override void HandleItem(Item request)
        {
            if (request.Type == ItemType.BackstageTicket)
            {
                var daysToSell = request.SellIn;
                var increment = daysToSell > 10 ? 1 : daysToSell > 5 ? 2 : 3;

                if (daysToSell >= 0)  //Kadangi pagal sena if'u makarona pirma padidindavo kokybe, paskui sumazindavo ir vel tikrindavo kiek dienu liko, esant neigiamam priskirdavo nuliui, tai dvigubas tikrinimas gaunasi ir cia, 
                {
                    request.IncreaseQualityValue(increment);
                }
                else
                {
                    request.Quality = 0;
                }
                request.SellIn--;
                if (request.SellIn < 0)
                {
                    request.Quality = 0;
                }
            }
            else
            {
                nextHandler.HandleItem(request);
            }
        }
    }
}
