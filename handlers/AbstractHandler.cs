﻿using csharpcore.models;

namespace csharpcore.handlers
{
    public abstract class AbstractHandler
    {
        protected AbstractHandler nextHandler;

        public void SetSuccessor(AbstractHandler nextHandler)
        {
            this.nextHandler = nextHandler;
        }

        public abstract void HandleItem(Item request);
    }


}
