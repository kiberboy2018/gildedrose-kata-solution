﻿using csharpcore.models;

namespace csharpcore.handlers
{
    public class StandardItemHandler : AbstractHandler
    {
        public override void HandleItem(Item request)
        {
            if (request.Type == ItemType.Standard)
            {
                request.SellIn--;
                request.DecreaseQualityValue();
            }
            else
            {
                nextHandler.HandleItem(request);
            }
        }
    }
}


